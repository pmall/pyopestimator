
pyOPestimator
=============

Estimation of the atmospheric PM10 oxidative potential based on an ensemble of
source apportionment study in France.

This work is part of the GetOP standOP ANR and of the PhD of Samuël Weber.

How to use it?
==============

Install it via `pip`
```
pip install pyOPestimator
```

and use its API to estimate the oxidative potential of a given PM10 concentration timeserie.

```python
import pyOPestimator

# Your PM10 dataframe with a Date column
dfpm = pyOPestimator.load_dataset("atmoaura_GRE-fr_pm10")

# Estimate the OP
dfop = pyOPestimator.get_op_from_pm10(dfpm, OPtype="DTTv", pm10colname="PM10")

dfop.set_index("Date")["totalOP"].plot()
```

How does it work?
=================

Method
------

Gathering several research program in France metropolitan area succeed to estimate a
climatology of the PM10 sources contribution to the ambient PM10 concentration.
We are then able to estimate a roughly monthly mean relative contribution of each sources to the PM10.

Then, for a given day of PM10 measurement, we can estimate the relative
contribution of a set of common sources found in the metropolitan territory.
We should keep in mind that this is a crude approximation since it does not
take into account for local specificities nor for variation over year of the
sources contributions.

Finally, thanks to the recent development of the scientific community, and notably
[Weber et al. 2020](#ref) for the France area, we can attribute an oxidative
potential (OP) of a set of sources. A simple multiplication end up with the
sources contribution to the oxidative potential of PM.

Pitfall
-------

This method is a crude, first order approximation of the sources contribution
to the ambient PM10 concentration. The learning set is representative of
urbanized area over France, for year between 2013 to 2018.

The oxidative potential of the sources does present some variation for a given source
at different location. In this method we only take into account for the mean value of them.

For this two reason, this method should be used cautiously and only give a first
idea of what could be the OP of your PM10 timeserie.
