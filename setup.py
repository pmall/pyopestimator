from distutils.core import setup

PACKAGES = [
    'pyOPestimator',
]


setup(
    name='pyOPestimator',
    version='0.1.1',
    packages=PACKAGES
)
